import React, { Component} from 'react';
import Topbar from './components/Topbar';

import './assets/App.css';

import arcusLogo from './assets/img/logo.png';
import reactLogo from './assets/img/logo.svg';
import map from './assets/img/snazzy-image.png';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons'
import { faCamera, faUser } from '@fortawesome/free-solid-svg-icons';

const style = {
  styleArrowBold: {
    borderTop: "10px solid transparent",
  },

  styleMarkerBold:{
    height: '70px',
    width: '70px'
  },

  repositionUserToMarker1:{
    top: '100px',
    left: '20px',
    color: 'cornflowerblue',
  },

  repositionUserToMarker2:{
    top: '112px',
    right: '135px',
    color: 'cornflowerblue',
  },

  repositionUserToMarker3:{
    left: '95px',
    top: '384px',
    color: 'cornflowerblue',
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userLocation: {
        lat: 0,
        lng: 0
      },
      marker1: {
        location: {
          lat: -7.669364,
          lng: 112.913552
        },
        isCloser: false,
        type: "ar",
        text: "face filter",
      },
      marker2: {
        location: {
          lat: -7.669128,
          lng: 112.913789,
        },
        isCloser: false,
        type: "sticker",
        text: "halloween girl",
      },
      marker3: {
        location: {
          lat: -7.669571,
          lng: 112.913492,
        },
        isCloser: false,
        type: "sticker",
        text: "halloween girl",
      },
      closestDistance: 0,
    }
  };
  
  componentDidMount = () => {
    if (navigator.geolocation) {
      navigator.geolocation.watchPosition((e) => this.showPosition(e));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  showPosition = (position) => {
    var window = {
      marker1 : this.state.marker1,
      marker2 : this.state.marker2,
      marker3 : this.state.marker3,
    }

    let userLocation = this.state.userLocation;
    userLocation.lat = position.coords.latitude;
    userLocation.lng = position.coords.longitude;

    var closestDistance = this.state.closestDistance;
    
    var distanceToMarker = [
      { lat:0, lng: 0 },
      { lat:0, lng: 0 },
      { lat:0, lng: 0 }
    ]

    if(userLocation.lat > window.marker1.location.lat){
      distanceToMarker[0].lat = userLocation.lat - window.marker1.location.lat; 
    } else {
      distanceToMarker[0].lat = window.marker1.location.lat - userLocation.lat; 
    }

    if(userLocation.lng > window.marker1.location.lng){
      distanceToMarker[0].lng = userLocation.lng - window.marker1.location.lng; 
    } else {
      distanceToMarker[0].lng = window.marker1.location.lng - userLocation.lng; 
    }

    if(userLocation.lat > window.marker2.location.lat){
      distanceToMarker[1].lat = userLocation.lat - window.marker2.location.lat; 
    } else {
      distanceToMarker[1].lat = window.marker2.location.lat - userLocation.lat; 
    }

    if(userLocation.lng > window.marker2.location.lng){
      distanceToMarker[1].lng = userLocation.lng - window.marker2.location.lng; 
    } else {
      distanceToMarker[1].lng = window.marker2.location.lng - userLocation.lng; 
    }

    if(userLocation.lat > window.marker3.location.lat){
      distanceToMarker[2].lat = userLocation.lat - window.marker3.location.lat; 
    } else {
      distanceToMarker[2].lat = window.marker3.location.lat - userLocation.lat; 
    }

    if(userLocation.lng > window.marker3.location.lng){
      distanceToMarker[2].lng = userLocation.lng - window.marker3.location.lng; 
    } else {
      distanceToMarker[2].lng = window.marker3.location.lng - userLocation.lng; 
    }

    var distance = [
      distanceToMarker[0].lat + distanceToMarker[0].lng,
      distanceToMarker[1].lat + distanceToMarker[1].lng,
      distanceToMarker[2].lat + distanceToMarker[2].lng,
    ];

    closestDistance = Math.min(...distance);
    var markerIndex = distance.indexOf(closestDistance)+1;
    if(distanceToMarker[markerIndex-1].lng <= 0.05 && distanceToMarker[markerIndex-1].lng <= 0.05){
      window['marker'+markerIndex].isCloser = true;
    }

    this.setState({
      userLocation, 
      marker1 : window.marker1, 
      marker2 : window.marker2,
      marker3 : window.marker3,
      closestDistance
    });
  }

  render() {
    return (
      <div className="App">
        <Topbar 
          arcusLogo={arcusLogo}
          reactLogo={reactLogo}
        />
        <div className="map-container">
          
          <div 
            style={
              this.state.marker1.isCloser ?
                style.styleArrowBold : null
            } 
            className="arrow-marker1">
          </div>
          <div 
            className="marker marker-1"
            style={
              this.state.marker1.isCloser ? 
              style.styleMarkerBold : null
            }
          >
            <FontAwesomeIcon icon={faCamera} />
            AR
          </div>
          <div className="arrow-marker2" 
            style={
            this.state.marker2.isCloser ?
              style.styleArrowBold : null
            }
          >
          </div>
          <div className="marker marker-2" 
            style={
              this.state.marker2.isCloser ? 
              style.styleMarkerBold : null
            }
          >
            <div className="circTxt" id="top"></div>
            <FontAwesomeIcon icon={faWhatsapp} />
            <div className="circling-text">
              WA
            </div>         
          </div>
          <div className="arrow-marker3"
            style={
              this.state.marker3.isCloser ?
                style.styleArrowBold : null
            } ></div>
          <div className="marker marker-3" 
            style={
              this.state.marker3.isCloser ? 
              style.styleMarkerBold : null}>
            <FontAwesomeIcon icon={faCamera} />
            AR
          </div>

          <div className="marker marker-user" 
            style={
                this.state.marker3.isCloser ? style.repositionUserToMarker3 
                  : this.state.marker2.isCloser ? style.repositionUserToMarker2 
                    : this.state.marker1.isCloser ? style.repositionUserToMarker1
                      : null}>
            <FontAwesomeIcon icon={faUser} />
          </div>
          <img className="map" src={map} alt="map"></img>
        </div>
      </div>
    );
  }
};

export default App;
