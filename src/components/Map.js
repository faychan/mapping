import React, { Component } from "react";

class Map extends Component {
	render() {
		return (
			<header>
				<img className="bars-icon" src={this.props.burgerMenu} alt="burger menu" />
				<img className="arcus-logo" src={this.props.logo} alt="arcus logo" />
			</header>
		);
	}
};

export default Map;