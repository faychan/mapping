import React, { Component } from "react";

class Topbar extends Component {
	render() {
		return (
			<header>
				<img className="react-logo" src={this.props.reactLogo} alt="react logo" />
				<img className="arcus-logo" src={this.props.arcusLogo} alt="arcus logo" />
			</header>
		);
	}
};

export default Topbar; 